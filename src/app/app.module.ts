import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MultilineService } from './services/multiline.service';
import { MultilineComponent } from './multiline/multiline.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    MultilineComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    MultilineService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
