import { Injectable } from '@angular/core';

import {HttpClient} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
// import sampleJson from '../../assets/data.json'

@Injectable({
  providedIn: 'root'
})
export class MultilineService {


  // private baseUrl = 'http://multilinechart.free.beeceptor.com'
  private data =  []
  
  constructor(private http:HttpClient) { }

    
  // getting data for multiline service 
  
  getMultilineData() {
    return this.data;
    // return this.http.get(this.baseUrl, {responseType: 'json'}); 
  }


}
